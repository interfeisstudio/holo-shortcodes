<?php
/* for shortcode widget  */
add_filter('widget_text', 'do_shortcode');

if( !function_exists('nvr_check_pagepost')){
	function nvr_check_pagepost(){
		global $post;
		
		if( is_404() || is_archive() || is_attachment() || is_search() ){
			$nvr_custom = false;
		}else{
			$nvr_custom = true;
		}
		
		return $nvr_custom;
	}
}

if( !function_exists('nvr_get_postid')){
	function nvr_get_postid(){
		global $post;
		
		if( is_home() ){
			$nvr_pid = get_option('page_for_posts');
		}elseif( function_exists( 'is_woocommerce' ) && is_shop() ){
			$nvr_pid = woocommerce_get_page_id( 'shop' );
		}elseif( function_exists( 'is_woocommerce' ) && is_product_category() ){
			$nvr_pid = woocommerce_get_page_id( 'shop' );
		}elseif( function_exists( 'is_woocommerce' ) && is_product_tag() ){
			$nvr_pid = woocommerce_get_page_id( 'shop' );
		}elseif(!nvr_check_pagepost()){
			$nvr_pid = 0;
		}else{
			$nvr_pid = get_the_ID();
		}
		
		return $nvr_pid;
	}
}

if( !function_exists('nvr_get_customdata')){
	function nvr_get_customdata($nvr_pid=""){
		global $post;
		
		if($nvr_pid!=""){
			$nvr_custom = get_post_custom($nvr_pid);
			return $nvr_custom;
		}
		
		if($nvr_pid==""){
			$nvr_pid = nvr_get_postid();
		}

		if( nvr_check_pagepost() ){
			$nvr_custom = get_post_custom($nvr_pid);
		}else{
			$nvr_custom = array();
		}
		
		return $nvr_custom;
	}
}

/*** WOOCOMMERCE CUSTOM FUNCTIONS ***/
if(!function_exists('nvr_productbox')){
	function nvr_productbox($postobj){
		
		if(!function_exists('is_woocommerce')){ return false; }
		global $post, $product, $woocommerce;
		
		setup_postdata($postobj);
		
		$licontent = "";
		ob_start();
		woocommerce_get_template_part( 'content', 'product' );
		$licontent .= ob_get_contents();
		ob_end_clean();
		
		
		return $licontent;
	}
}
if(!function_exists('nvr_productquery')){
	function nvr_productquery($number=12, $type, $cat=''){
		
		if(!function_exists('is_woocommerce')){ return false; }
		global $woocommerce;
		
		
		if(!is_numeric($number)){
			$number = 12;
		}
		
		if($type == 'featured'){
			/**********QUERY FOR FEATURED PRODUCT**********/
			$query_args = array('posts_per_page' => $number, 'no_found_rows' => 1, 'post_status' => 'publish', 'post_type' => 'product' );
			
			$query_args['meta_query'] = array();
		
			$query_args['meta_query'][] = array(
				'key' => '_featured',
				'value' => 'yes'
			);
			$query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
			$query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
			/**********END QUERY FOR FEATURED PRODUCT**********/
		}elseif($type == 'onsale'){
			/**********QUERY FOR ONSALE PRODUCT**********/
			$query_args = array('posts_per_page' => $number, 'no_found_rows' => 1, 'post_status' => 'publish', 'post_type' => 'product' );
			
			$nvr_ids_on_sale    = wc_get_product_ids_on_sale();
			$nvr_ids_on_sale[]  = 0; 
			$query_args['post__in'] = $nvr_ids_on_sale;
			/**********END QUERY FOR ONSALE PRODUCT**********/
		}elseif($type == 'top-rated'){
			/**********QUERY FOR TOP-RATED PRODUCT**********/
			add_filter( 'posts_clauses',  array( $woocommerce->query, 'order_by_rating_post_clauses' ) );
		
			$query_args = array('posts_per_page' => $number, 'no_found_rows' => 1, 'post_status' => 'publish', 'post_type' => 'product' );
		
			$query_args['meta_query'] = array();
		
			$query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
			$query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
			
			/**********END QUERY FOR TOP-RATED PRODUCT**********/
		}elseif($type == 'best-selling'){
			/**********QUERY FOR BEST SELLING PRODUCT**********/
			$query_args = array(
				'posts_per_page' => $number,
				'post_status' 	 => 'publish',
				'post_type' 	 => 'product',
				'meta_key' 		 => 'total_sales',
				'orderby' 		 => 'meta_value_num',
				'no_found_rows'  => 1,
			);
		
			$query_args['meta_query'] = array();
		
			if ( isset( $instance['hide_free'] ) && 1 == $instance['hide_free'] ) {
				$query_args['meta_query'][] = array(
					'key'     => '_price',
					'value'   => 0,
					'compare' => '>',
					'type'    => 'DECIMAL',
				);
			}
		
			$query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
			$query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
			/**********END QUERY FOR BEST SELLING PRODUCT**********/
		}elseif($type == 'latest'){
			$query_args = array('posts_per_page' => $number, 'no_found_rows' => 1, 'orderby' => 'date', 'order' => 'DESC', 'post_status' => 'publish', 'post_type' => 'product' );
		}
		
		if($cat){
			$cat = explode(",",$cat);
			$query_args['tax_query'] = array(
				array(
					'taxonomy' => 'product_cat',
					'field' => 'slug',
					'terms' => $cat
				)
			);
		}
		
		return $query_args;
	}
}

if(!function_exists('holo_wc_product_post_class')){ 
    function holo_wc_product_post_class( $classes, $class = '', $post_id = '' ) {
        if ( ! $post_id || 'product' !== get_post_type( $post_id ) ) {
            return $classes;
        }

        // add category slugs
        $categories = get_the_terms( $post_id, 'product_cat' );
        if ( ! empty( $categories ) ) {
            foreach ( $categories as $key => $value ) {
                $classes[] = 'product-cat-' . $value->slug;
            }
        }
        return $classes;
    }
    add_filter( 'post_class', 'holo_wc_product_post_class', 21, 3 );
}

if(!function_exists("nvr_string_limit_char")){
	function nvr_string_limit_char($nvr_excerpt, $nvr_substr=0, $nvr_strmore = "..."){
		$nvr_string = strip_tags(str_replace('...', '', $nvr_excerpt));
		if ($nvr_substr>0) {
			$nvr_string = substr($nvr_string, 0, $nvr_substr);
		}
		if(strlen($nvr_excerpt)>=$nvr_substr){
			$nvr_string .= $nvr_strmore;
		}
		return $nvr_string;
	}
}
// The excerpt based on words
if(!function_exists("nvr_string_limit_words")){
	function nvr_string_limit_words($nvr_string, $nvr_word_limit){
	  $nvr_words = explode(' ', $nvr_string, ($nvr_word_limit + 1));
	  if(count($nvr_words) > $nvr_word_limit)
	  array_pop($nvr_words);
	  
	  return implode(' ', $nvr_words);
	}
}

// Actual processing of the shortcode happens here
function nvr_pre_shortcode( $content ) {
    global $shortcode_tags;
 
    // Backup current registered shortcodes and clear them all out
    $orig_shortcode_tags = $shortcode_tags;
 
    // Do the shortcode (only the one above is registered)
    $content = do_shortcode( $content );
 
    // Put the original shortcodes back
    $shortcode_tags = $orig_shortcode_tags;
 
    return $content;
}
?>