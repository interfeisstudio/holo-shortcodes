<?php
// =============================== Holo Banner Image Widget ======================================
class Holo_BannerImageWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_bannerimg', 'description' => esc_html__('Holo - Banner Image', 'holo-shortcodes') );
		parent::__construct('holo-bannerimg-widget', esc_html__('Holo - Banner Image','holo-shortcodes'), $widget_ops);
	}

	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$src      	= apply_filters('widget_holo_product_filter_src', empty($instance['src']) ? '' : $instance['src']);
		$class     	= apply_filters('widget_holo_product_filter_class', empty($instance['class']) ? '' : $instance['class']);
		$link      	= apply_filters('widget_holo_product_filter_link', empty($instance['link']) ? '' : $instance['link']);
    $margin    	= apply_filters('widget_holo_product_filter_margin', empty($instance['margin']) ? '' : $instance['margin']);
    $padding   	= apply_filters('widget_holo_product_filter_padding', empty($instance['padding']) ? '' : $instance['padding']);
    $content    = apply_filters('widget_holo_product_filter_content', empty($instance['content']) ? '' : $instance['content']);
		$subcontent    = apply_filters('widget_holo_product_filter_subcontent', empty($instance['subcontent']) ? '' : $instance['subcontent']);

        $scparams = '';
				if(trim($src)!=''){
            $scparams .= ' src="'.esc_attr($src).'"';
        }
				if(trim($class)!=''){
            $scparams .= ' class="'.esc_attr($class).'"';
        }
        if(trim($link)!=''){
            $scparams .= ' link="'.esc_attr($link).'"';
        }

        if(trim($margin)!=''){
            $scparams .= ' margin="'.esc_attr($margin).'"';
        }

        if(trim($padding)!=''){
            $scparams .= ' padding="'.esc_attr($padding).'"';
        }

				if(trim($subcontent) != ''){
						$scparams .= ' subcontent="'.$subcontent.'"';
				}

        echo do_shortcode('[bannerimg '.$scparams.']'.$content.'[/bannerimg]');
	}

	function update($new_instance, $old_instance) {
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
				$instance['src'] = (isset($instance['src']))? $instance['src'] : "";
				$instance['link'] = (isset($instance['link']))? $instance['link'] : "";
				$instance['margin'] = (isset($instance['margin']))? $instance['margin'] : "";
				$instance['padding'] = (isset($instance['padding']))? $instance['padding'] : "";
				$instance['class'] = (isset($instance['class']))? $instance['class'] : "";
				$instance['content'] = (isset($instance['content']))? $instance['content'] : "";
				$instance['subcontent'] = (isset($instance['subcontent']))? $instance['subcontent'] : "";


				$src = esc_attr($instance['src']);
				$link = esc_attr($instance['link']);
        $margin = esc_attr($instance['margin']);
				$padding = esc_attr($instance['padding']);
				$class = esc_attr($instance['class']);
				$content = $instance['content'];
				$subcontent = $instance['subcontent'];

        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('src') ); ?>"><?php esc_html_e('Image Source URL:', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('src') ); ?>" name="<?php echo esc_attr( $this->get_field_name('src') ); ?>" type="text" value="<?php echo esc_attr( $src ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('link') ); ?>"><?php esc_html_e('Link:', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('link') ); ?>" name="<?php echo esc_attr( $this->get_field_name('link') ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('margin') ); ?>"><?php esc_html_e('Margin (px):', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('margin') ); ?>" name="<?php echo esc_attr( $this->get_field_name('margin') ); ?>" type="text" value="<?php echo esc_attr( $margin ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('padding') ); ?>"><?php esc_html_e('Padding (px):', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('padding') ); ?>" name="<?php echo esc_attr( $this->get_field_name('padding') ); ?>" type="text" value="<?php echo esc_attr( $padding ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('class') ); ?>"><?php esc_html_e('Custom Class (Optional):', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('class') ); ?>" name="<?php echo esc_attr( $this->get_field_name('class') ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('subcontent') ); ?>"><?php esc_html_e('Subcontent:', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('subcontent') ); ?>" name="<?php echo esc_attr( $this->get_field_name('subcontent') ); ?>" type="text" value="<?php echo esc_attr( $subcontent ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('content') ); ?>"><?php esc_html_e('Content:', 'holo-shortcodes'); ?> <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id('content') ); ?>" name="<?php echo esc_attr( $this->get_field_name('content') ); ?>"><?php echo esc_attr( $content ); ?></textarea></label></p>
        <?php
    }
}
?>
