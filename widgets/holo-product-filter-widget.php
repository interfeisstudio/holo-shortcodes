<?php
// =============================== Holo Product Filter Widget ======================================
class Holo_ProductFilterWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_product_filter', 'description' => esc_html__('Holo - Product Filter', 'holo-shortcodes') );
		parent::__construct('holo-product-filter-widget', esc_html__('Holo - Product Filter','holo-shortcodes'), $widget_ops);
	}

	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$id      = apply_filters('widget_holo_product_filter_id', empty($instance['id']) ? '' : $instance['id']);
		$class      = apply_filters('widget_holo_product_filter_class', empty($instance['class']) ? '' : $instance['class']);
		$title      = apply_filters('widget_holo_product_filter_title', empty($instance['title']) ? '' : $instance['title']);
    $nofilter    = apply_filters('widget_holo_product_filter_nofilter', empty($instance['nofilter']) ? '' : $instance['nofilter']);
    $cat        = apply_filters('widget_holo_product_filter_cat', empty($instance['cat']) ? '' : $instance['cat']);
    $type        = apply_filters('widget_holo_product_filter_type', empty($instance['type']) ? '' : $instance['type']);
    $col        = apply_filters('widget_holo_product_filter_col', empty($instance['col']) ? '' : $instance['col']);
    $showposts  = apply_filters('widget_holo_product_filter_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);

        $scparams = '';
				if(trim($id)!=''){
            $scparams .= ' id="'.esc_attr($id).'"';
        }
				if(trim($class)!=''){
            $scparams .= ' class="'.esc_attr($class).'"';
        }
        if(trim($title)!=''){
            $scparams .= ' title="'.esc_attr($title).'"';
        }

        if(trim($nofilter)!=''){
            $scparams .= ' nofilter="'.esc_attr($nofilter).'"';
        }

        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }

        if(trim($col)!=''){
            $scparams .= ' col="'.esc_attr($col).'"';
        }

				if(trim($type)!=''){
            $scparams .= ' type="'.esc_attr($type).'"';
        }

        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }

        echo do_shortcode('[product_filter '.$scparams.']');
	}

	function update($new_instance, $old_instance) {
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
				$instance['id'] = (isset($instance['id']))? $instance['id'] : "";
				$instance['class'] = (isset($instance['class']))? $instance['class'] : "";
				$instance['title'] = (isset($instance['title']))? $instance['title'] : "";
				$instance['col'] = (isset($instance['col']))? $instance['col'] : "";
				$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
				$instance['type'] = (isset($instance['type']))? $instance['type'] : "";
		    $instance['nofilter'] = (isset($instance['nofilter']))? $instance['nofilter'] : "";
		    $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";

        $cols = array(
            '2' => __('2 Columns', 'holo-shortcodes'),
            '3' => __('3 Columns', 'holo-shortcodes'),
            '4' => __('4 Columns', 'holo-shortcodes')
        );

        $grids = array(
            'featured' => __('Featured', 'holo-shortcodes'),
            'latest' => __('Latest', 'holo-shortcodes'),
            'top-rated' => __('Top Rated', 'holo-shortcodes'),
            'best-selling' => __('Best Selling', 'holo-shortcodes'),
            'on-sale' => __('On Sale', 'holo-shortcodes')
        );

        $arrsval = array(
            'yes' => __('Yes', 'holo-shortcodes'),
            'no' => __('No', 'holo-shortcodes')
        );

				$id = esc_attr($instance['id']);
				$class = esc_attr($instance['class']);
        $title = esc_attr($instance['title']);
				$col = esc_attr($instance['col']);
				$cat = esc_attr($instance['cat']);
        $type = esc_attr($instance['type']);
        $nofilter = esc_attr($instance['nofilter']);
				$showposts = esc_attr($instance['showposts']);


        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Product Category Slug:', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('col') ); ?>"><?php esc_html_e('Number of Columns:', 'holo-shortcodes' ); ?>
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('col') ); ?>" name="<?php echo esc_attr( $this->get_field_name('col') ); ?>">
                    <?php foreach($cols as $colval => $colname){ ?>
                        <?php $selected = ($colval==$col)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $colval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $colname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('type') ); ?>"><?php esc_html_e('Type:', 'holo-shortcodes' ); ?>
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('type') ); ?>" name="<?php echo esc_attr( $this->get_field_name('type') ); ?>">
                    <?php foreach($grids as $gridval => $gridname){ ?>
                        <?php $selected = ($gridval==$type)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $gridval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $gridname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('nofilter') ); ?>"><?php esc_html_e('No Filter:', 'holo-shortcodes' ); ?>
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('nofilter') ); ?>" name="<?php echo esc_attr( $this->get_field_name('nofilter') ); ?>">
                    <?php foreach($arrsval as $arrval => $arrname ){ ?>
                        <?php $selected = ($arrval==$nospace)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $arrval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $arrname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('id') ); ?>"><?php esc_html_e('Custom ID (Optional):', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('id') ); ?>" name="<?php echo esc_attr( $this->get_field_name('id') ); ?>" type="text" value="<?php echo esc_attr( $id ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('class') ); ?>"><?php esc_html_e('Custom Class (Optional):', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('class') ); ?>" name="<?php echo esc_attr( $this->get_field_name('class') ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>" /></label></p>
        <?php
    }
}
?>
