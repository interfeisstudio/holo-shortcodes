<?php
// =============================== Holo Product Carousel Widget ======================================
class Holo_ProductCarouselWidget extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_holo_product_carousel', 'description' => esc_html__('Holo - Product Carousel', 'holo-shortcodes') );
		parent::__construct('holo-product-carousel-widget', esc_html__('Holo - Product Carousel','holo-shortcodes'), $widget_ops);
	}

	function widget( $args, $instance ) {
		global $wpdb, $comments, $comment;

		extract($args, EXTR_SKIP);
		$class      = apply_filters('widget_holo_product_filter_class', empty($instance['class']) ? '' : $instance['class']);
		$title      = apply_filters('widget_holo_product_filter_title', empty($instance['title']) ? '' : $instance['title']);
    $cat        = apply_filters('widget_holo_product_filter_cat', empty($instance['cat']) ? '' : $instance['cat']);
    $type        = apply_filters('widget_holo_product_filter_type', empty($instance['type']) ? '' : $instance['type']);
    $showposts  = apply_filters('widget_holo_product_filter_showposts', empty($instance['showposts']) ? '' : $instance['showposts']);

        $scparams = '';
				if(trim($class)!=''){
            $scparams .= ' class="'.esc_attr($class).'"';
        }
        if(trim($title)!=''){
            $scparams .= ' title="'.esc_attr($title).'"';
        }
        if(trim($cat)!=''){
            $scparams .= ' cat="'.esc_attr($cat).'"';
        }
				if(trim($type)!=''){
            $scparams .= ' type="'.esc_attr($type).'"';
        }
        if(trim($showposts)!=''){
            $scparams .= ' showposts="'.esc_attr($showposts).'"';
        }

        echo do_shortcode('[product_carousel '.$scparams.']');
	}

	function update($new_instance, $old_instance) {
        return $new_instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {
				$instance['class'] = (isset($instance['class']))? $instance['class'] : "";
				$instance['title'] = (isset($instance['title']))? $instance['title'] : "";
				$instance['cat'] = (isset($instance['cat']))? $instance['cat'] : "";
				$instance['type'] = (isset($instance['type']))? $instance['type'] : "";
		    $instance['showposts'] = (isset($instance['showposts']))? $instance['showposts'] : "";

        $grids = array(
            'featured' => __('Featured', 'holo-shortcodes'),
            'latest' => __('Latest', 'holo-shortcodes'),
            'top-rated' => __('Top Rated', 'holo-shortcodes'),
            'best-selling' => __('Best Selling', 'holo-shortcodes'),
            'on-sale' => __('On Sale', 'holo-shortcodes')
        );

        $arrsval = array(
            'yes' => __('Yes', 'holo-shortcodes'),
            'no' => __('No', 'holo-shortcodes')
        );

				$class = esc_attr($instance['class']);
        $title = esc_attr($instance['title']);
				$cat = esc_attr($instance['cat']);
        $type = esc_attr($instance['type']);
				$showposts = esc_attr($instance['showposts']);


        ?>
            <p><label for="<?php echo esc_attr( $this->get_field_id('title') ); ?>"><?php esc_html_e('Title:', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('cat') ); ?>"><?php esc_html_e('Product Category Slug:', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('cat') ); ?>" name="<?php echo esc_attr( $this->get_field_name('cat') ); ?>" type="text" value="<?php echo esc_attr( $cat ); ?>" /></label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('type') ); ?>"><?php esc_html_e('Type:', 'holo-shortcodes' ); ?>
                <select class="widefat" id="<?php echo esc_attr( $this->get_field_id('type') ); ?>" name="<?php echo esc_attr( $this->get_field_name('type') ); ?>">
                    <?php foreach($grids as $gridval => $gridname){ ?>
                        <?php $selected = ($gridval==$type)? 'selected="selected"' : ''; ?>
                        <option value="<?php echo esc_attr( $gridval ); ?>" <?php echo $selected; ?>><?php echo esc_html( $gridname ); ?></option>
                    <?php }?>
                </select>
            </label></p>

            <p><label for="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>"><?php esc_html_e('Showposts:', 'holo-shortcodes' ); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('showposts') ); ?>" name="<?php echo esc_attr( $this->get_field_name('showposts') ); ?>" type="text" value="<?php echo esc_attr( $showposts ); ?>" /></label></p>

						<p><label for="<?php echo esc_attr( $this->get_field_id('class') ); ?>"><?php esc_html_e('Custom Class (Optional):', 'holo-shortcodes'); ?> <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('class') ); ?>" name="<?php echo esc_attr( $this->get_field_name('class') ); ?>" type="text" value="<?php echo esc_attr( $class ); ?>" /></label></p>
        <?php
    }
}
?>
