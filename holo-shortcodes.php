<?php
/*
Plugin Name: Holo Essential Shortcodes
Plugin URI: http://www.interfeis.com/
Description: Holo Essential Shortcodes is a wordpress plugin that contain all of holotheme shortcodes.
Version: 1.1.3
Author: interfeis
Author URI: http://www.interfeis.com/
License: GPL
*/

/*  Copyright 2015  Interfeis

    Holo Testimonial is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

//to block direct access
if ( ! defined( 'ABSPATH' ) )
	die( "Can't load this file directly" );

//global variable for this plugin
$pathinfo	= pathinfo(__FILE__);
$plugins_dir = dirname( __FILE__ );

include_once( $plugins_dir.'/widgets/holo-product-carousel-widget.php' );
include_once( $plugins_dir.'/widgets/holo-product-filter-widget.php' );
include_once( $plugins_dir.'/widgets/holo-bannerimg-widget.php' );

if(file_exists(plugin_dir_path(dirname(__FILE__)).'/js_composer/include/classes/shortcodes/shortcodes.php')){
    include_once plugin_dir_path(dirname(__FILE__)).'/js_composer/include/classes/shortcodes/shortcodes.php';
}

class Holo_Shortcode{

	var $imagesizes;
	var	$langval;
	var	$version;
	var $defaultattr;
	var $postslug;
	var $taxonomslug;
	var $posttype;
	var $posttaxonomy;

	function __construct(){

		// Register the options menu
		add_action('admin_init', 'flush_rewrite_rules');
		add_action('init', array($this, 'holo_pf_action_init'));

		$this->holo_includes();

		add_shortcode( 'bannerimg', 'nvr_bannerimg' );

		add_shortcode( 'bordercustom', 'nvr_borderseparator' );

		add_shortcode('row', 'nvr_row');
		add_shortcode('one_half', 'nvr_one_half');
		add_shortcode('one_third', 'nvr_one_third');
		add_shortcode('one_fourth', 'nvr_one_fourth');
		add_shortcode('one_fifth', 'nvr_one_fifth');
		add_shortcode('one_sixth', 'nvr_one_sixth');
		add_shortcode('two_third', 'nvr_two_third');
		add_shortcode('two_fourth', 'nvr_two_fourth');
		add_shortcode('two_fifth', 'nvr_two_fifth');
		add_shortcode('two_sixth', 'nvr_two_sixth');
		add_shortcode('three_fourth', 'nvr_three_fourth');
		add_shortcode('three_fifth', 'nvr_three_fifth');
		add_shortcode('three_sixth', 'nvr_three_sixth');
		add_shortcode('four_fifth', 'nvr_four_fifth');
		add_shortcode('four_sixth', 'nvr_four_sixth');
		add_shortcode('five_sixth', 'nvr_five_sixth');

		add_shortcode('content_title', 'nvr_content_title');

		add_shortcode( 'counters', 'nvr_counters' );

		add_shortcode( 'dropcap', 'nvr_dropcap' );

		add_shortcode( 'featuredgallery', 'nvr_featuredgallery' );

		add_shortcode( 'featuredslider', 'nvr_featuredslider' );

		add_shortcode( 'heading', 'nvr_heading' );

		add_shortcode( 'highlight', 'nvr_highlight' );

		add_shortcode( 'hostingtable', 'nvr_hostingtable' );

		add_shortcode( 'iconcontainer', 'nvr_iconcontainer' );

		add_shortcode('loginform','nvr_login_form');

		add_shortcode( 'meter', 'nvr_meter' );

		add_shortcode('pre', 'nvr_pre');
		add_shortcode('code', 'nvr_code');

		add_shortcode( 'product_carousel', 'nvr_productcarousel' );
		add_shortcode( 'product_filter', 'nvr_productfilter' );

		add_shortcode( 'pullquote', 'nvr_pullquote' );
		add_shortcode( 'blockquote', 'nvr_blockquote' );

		add_shortcode( 'recent_posts', 'nvr_recentposts' );

		add_shortcode('registerform','nvr_register_form');

		add_shortcode('separator', 'nvr_separator');
		add_shortcode('spacer', 'nvr_spacer');
		add_shortcode('clearfix', 'nvr_clearfixfloat');

		add_shortcode( 'sliders', 'nvr_sliders' );
		add_shortcode( 'slide', 'nvr_slide' );

		add_shortcode('tabs', 'nvr_tab');

		add_shortcode('toggles', 'nvr_toggles');
		add_shortcode('toggle', 'nvr_toggle');

		add_shortcode('socialnetwork','nvr_socialnetwork');

		add_shortcode("nvr_grid_img","nvr_grid_image");
		add_shortcode("nvr_grid_info","nvr_grid_information");
		add_shortcode("nvr_grid_product","nvr_grid_product");

        add_shortcode('nvr_pinboard', 'nvr_pinboard');
		add_shortcode('nvr_pin', 'nvr_pinboard_pin');

		$this->version		= $this->holo_plugin_version();
	}

	//Get the version of portfolio
	function holo_plugin_version(){
		$this->version = "1.0";

		return $this->version;
	}

	function holo_lang(){
		$thelang = 'holo';
		return $thelang;
	}

	function holo_shortname(){
		$theshortname = 'holo';
		return $theshortname;
	}

	function holo_initial(){
		$theinitial = 'nvr';
		return $theinitial;
	}

	function holo_pf_md5hash($str = ''){
		return md5($str);
	}

	function holo_includes(){

		$pluginpath = plugin_dir_path( __FILE__ );

		require_once( $pluginpath.'holo-functions.php' );
		require_once( $pluginpath.'shortcodes/bannerimg.php' );
		require_once( $pluginpath.'shortcodes/borderseparator.php' );
		require_once( $pluginpath.'shortcodes/columns.php' );
		require_once( $pluginpath.'shortcodes/content-title.php' );
		require_once( $pluginpath.'shortcodes/counters.php' );
		require_once( $pluginpath.'shortcodes/dropcap.php' );
		require_once( $pluginpath.'shortcodes/featuredgallery.php' );
		require_once( $pluginpath.'shortcodes/featuredslider.php' );
		require_once( $pluginpath.'shortcodes/heading.php' );
		require_once( $pluginpath.'shortcodes/highlight.php' );
		require_once( $pluginpath.'shortcodes/hostingtable.php' );
		require_once( $pluginpath.'shortcodes/iconcontainer.php' );
		require_once( $pluginpath.'shortcodes/loginform.php' );
		require_once( $pluginpath.'shortcodes/meter.php' );
		require_once( $pluginpath.'shortcodes/pre.php' );
		require_once( $pluginpath.'shortcodes/products.php' );
		require_once( $pluginpath.'shortcodes/quote.php' );
		require_once( $pluginpath.'shortcodes/recentposts.php' );
		require_once( $pluginpath.'shortcodes/registerform.php' );
		require_once( $pluginpath.'shortcodes/separator.php' );
		require_once( $pluginpath.'shortcodes/sliders.php' );
		require_once( $pluginpath.'shortcodes/socialnetwork.php' );
		require_once( $pluginpath.'shortcodes/tabs.php' );
		require_once( $pluginpath.'shortcodes/toggles.php' );
		require_once( $pluginpath.'shortcodes/gridimg.php' );
		require_once( $pluginpath.'shortcodes/gridinfo.php' );
		require_once( $pluginpath.'shortcodes/gridproduct.php' );
        require_once( $pluginpath.'shortcodes/pinboard.php' );
	}

	function holo_pf_action_init(){
		// only hook up these filters if we're in the admin panel, and the current user has permission
		// to edit posts and pages

        add_image_size( 'blog-post-image', 900, 480, true ); // Blog Image

		$version = $this->holo_plugin_version();
		if(!is_admin()){
			wp_register_script('appear', plugin_dir_url( __FILE__ ) .'js/appear.js', array('jquery'), '1.0', true);
			wp_enqueue_script('appear');

			wp_register_script('isotope', plugin_dir_url( __FILE__ ).'js/jquery.isotope.min.js', array('jquery'), '1.0', true);
			wp_enqueue_script('isotope');

			wp_register_script('prettyPhoto', plugin_dir_url( __FILE__ ).'js/jquery.prettyPhoto.js', array('jquery'), '3.0', true);
			wp_enqueue_script('prettyPhoto');

			wp_register_script('infinitescroll', plugin_dir_url( __FILE__ ).'js/jquery.infinitescroll.js', array('jquery'), '2.0b2', true);
			wp_enqueue_script('infinitescroll');

			wp_register_script('imagesloaded', plugin_dir_url( __FILE__ ).'js/imagesloaded.pkgd.min.js', array('jquery'), '3.0.4', true);
			wp_enqueue_script('imagesloaded');

			wp_register_script('perfect-scrollbar', plugin_dir_url( __FILE__ ) .'js/perfect-scrollbar.with-mousewheel.min.js', array('jquery'), '0.4.9', true);
			wp_enqueue_script('perfect-scrollbar');

			wp_register_script('flexslider', plugin_dir_url( __FILE__ ).'js/jquery.flexslider-min.js', array('jquery'), '1.8', true);
			wp_enqueue_script('flexslider');

			wp_register_script('countTo', plugin_dir_url( __FILE__ ) .'js/jquery.countTo.js', array('jquery'), '1.0', true);
			wp_enqueue_script('countTo');

			wp_register_script('holo_customshortcodes', plugin_dir_url( __FILE__ ).'js/holoshortcodes.js', array('jquery'), '1.8', true);
			wp_enqueue_script('holo_customshortcodes');


			$nvr_localvar = array(
				'pluginurl'					=> plugin_dir_url( __FILE__ )
			);
			wp_localize_script( 'holo_customshortcodes', 'nvrshortcodeslocal_var', $nvr_localvar );

			//Register and use this plugin main CSS
			wp_register_style('holo_skeleton', plugin_dir_url( __FILE__ ).'css/1140.css', 'normalize', '', 'screen, all');
			wp_enqueue_style('holo_skeleton');

			wp_register_style('flexslider', plugin_dir_url( __FILE__ ).'css/flexslider.css', '', '', 'screen, all');
			wp_enqueue_style('flexslider');

			wp_register_style('prettyPhoto', plugin_dir_url( __FILE__ ).'css/prettyPhoto.css', '', '', 'screen, all');
			wp_enqueue_style('prettyPhoto');

			wp_register_style('perfect-scrollbar', plugin_dir_url( __FILE__ ) . 'css/perfect-scrollbar.min.css', 'nvr_skeleton', '', 'screen, all');
			wp_enqueue_style('perfect-scrollbar');

			wp_register_style('holo_custom-shortcodes', plugin_dir_url( __FILE__ ).'css/holoshortcodes.css', '', '', 'screen, all');
			wp_enqueue_style('holo_custom-shortcodes');
		}
	}

	// The excerpt based on character
	function holo_pf_limit_char($excerpt, $substr=0, $strmore = "..."){
		$string = strip_tags(str_replace('...', '...', $excerpt));
		if ($substr>0) {
			$string = substr($string, 0, $substr);
		}
		if(strlen($excerpt)>=$substr){
			$string .= $strmore;
		}
		return $string;
	}

}

$theshortcode = new Holo_Shortcode();

function holo_vc_map(){
    vc_map(
        array(
            "name" => __( "Grid Image Content", 'holo-shortcodes'),
            "base" => "nvr_grid_img",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
								array(
										"type" => "textfield",
										"class" => "",
										"admin_label" => true,
										"heading" => __( "Height", 'holo-shortcodes' ),
										"param_name" => "height",
										"value" => "",
										"description" => __( "Height for your grid image. ( Default is 616px )", 'holo-shortcodes' )
								),
								array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Title", 'holo-shortcodes' ),
                    "param_name" => "title",
                    "value" => "",
                    "description" => __( "Title for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Subtitle", 'holo-shortcodes' ),
                    "param_name" => "subtitle",
                    "value" => "",
                    "description" => __( "Subtitle for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Text on Link", 'holo-shortcodes' ),
                    "param_name" => "linktext",
                    "value" => "",
                    "description" => __( "link text for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Link URL", 'holo-shortcodes' ),
                    "param_name" => "link",
                    "value" => '',
                    "description" => __( "link for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __( 'Background Color', 'holo-shortcodes' ),
                    'param_name' => 'bgcolor',
                    "value" => '',
                    "description" => __( "Background color for your grid.", 'holo-shortcodes' )
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => __( 'Background Image', 'holo-shortcodes' ),
                    'param_name' => 'bgimage',
                    "value" => '',
                    "description" => __( "Choose the background image for your grid.", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Grid Info Content", 'holo-shortcodes'),
            "base" => "nvr_grid_info",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Title", 'holo-shortcodes' ),
                    "param_name" => "title",
                    "value" => "",
                    "description" => __( "Title for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Subtitle", 'holo-shortcodes' ),
                    "param_name" => "subtitle",
                    "value" => "",
                    "description" => __( "Subtitle for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => __( 'Image', 'holo-shortcodes' ),
                    'param_name' => 'image',
                    "value" => '',
                    "description" => __( "Choose the image for your grid.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Link URL", 'holo-shortcodes' ),
                    "param_name" => "link",
                    "value" => '',
                    "description" => __( "Link for your grid image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Link Text", 'holo-shortcodes' ),
                    "param_name" => "linktext",
                    "value" => '',
                    "description" => __( "Text for your link button on your grid info.", 'holo-shortcodes' )
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __( 'Background Color', 'holo-shortcodes' ),
                    'param_name' => 'bgcolor',
                    "value" => '#fbfbfb',
                    "description" => __( "Background color for your grid.", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Grid Product", 'holo-shortcodes'),
            "base" => "nvr_grid_product",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "admin_label" => true,
                    "class" => "",
                    "heading" => __( "Product ID or Slug", 'holo-shortcodes' ),
                    "param_name" => "productid",
                    "value" => "",
                    "description" => __( "Input your product id or product slug.", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Banner Image", 'holo-shortcodes'),
            "base" => "bannerimg",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Image Source URL", 'holo-shortcodes' ),
                    "param_name" => "src",
                    "value" => "",
                    "description" => __( "Input the URL of your image.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Banner Link", 'holo-shortcodes' ),
                    "param_name" => "link",
                    "value" => "",
                    "description" => __( "Input the link of your banner.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Margin", 'holo-shortcodes' ),
                    "param_name" => "margin",
                    "value" => "",
                    "description" => __( "Input the margin of your banner.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Padding", 'holo-shortcodes' ),
                    "param_name" => "padding",
                    "value" => "",
                    "description" => __( "Input the padding of your banner.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Class", 'holo-shortcodes' ),
                    "param_name" => "class",
                    "value" => "",
                    "description" => __( "Input the custom class of your banner.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textarea_html",
                    "class" => "",
                    "heading" => __( "Content", "holo-shortcodes" ),
                    "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                    "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "holo-shortcodes" ),
                    "description" => __( "Enter your content.", "holo-shortcodes" )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Product Filter", 'holo-shortcodes'),
            "base" => "product_filter",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Custom ID", 'holo-shortcodes' ),
                    "param_name" => "id",
                    "value" => "",
                    "description" => __( "Input your custom ID. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Custom Class", 'holo-shortcodes' ),
                    "param_name" => "class",
                    "value" => "",
                    "description" => __( "Input your custom class. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Title", 'holo-shortcodes' ),
                    "param_name" => "title",
                    "value" => "",
                    "description" => __( "Input your title. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Columns", 'holo-shortcodes' ),
                    "param_name" => "col",
                    "value" => array(
                        __('Two Columns', 'holo-shortcodes') => "2" ,
                        __('Three Columns', 'holo-shortcodes') => "3",
                        __('Four Columns', 'holo-shortcodes') => "4",
                    ),
                    "std" => "4",
                    "description" => __( "Select the columns.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Product Category Slug", 'holo-shortcodes' ),
                    "param_name" => "cat",
                    "value" => "",
                    "description" => __( "Input the product category slugs. Please use comma-separated to use more than one category slug", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "admin_label" => true,
                    "heading" => __( "Type", 'holo-shortcodes' ),
                    "param_name" => "type",
                    "value" => array(
                        __('Featured', 'holo-shortcodes') => "featured",
                        __('Best Selling', 'holo-shortcodes') => "best-selling",
                        __('Top Rated', 'holo-shortcodes') => "top-rated",
                        __('Latest', 'holo-shortcodes') => "latest",
                        __('On Sale', 'holo-shortcodes') => "onsale"
                    ),
                    "description" => __( "Select the type of query.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => __( "Remove Filter?", 'holo-shortcodes' ),
                    "param_name" => "nofilter",
                    "value" => array(
                        __('No', 'holo-shortcodes') => "no",
                        __('Yes', 'holo-shortcodes') => "yes"
                    ),
                    "description" => __( "", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Show Posts", 'holo-shortcodes' ),
                    "param_name" => "showposts",
                    "value" => '',
                    "description" => __( "Input the number of product that you want to display.", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Product Carousel", 'holo-shortcodes'),
            "base" => "product_carousel",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Custom Class", 'holo-shortcodes' ),
                    "param_name" => "class",
                    "value" => "",
                    "description" => __( "Input your custom class. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Title", 'holo-shortcodes' ),
                    "param_name" => "title",
                    "admin_label" => true,
                    "value" => "",
                    "description" => __( "Input your title. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Product Category Slug", 'holo-shortcodes' ),
                    "param_name" => "cat",
                    "admin_label" => true,
                    "value" => "",
                    "description" => __( "Input the product category slugs. Please use comma-separated to use more than one category slug", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => __( "Type", 'holo-shortcodes' ),
                    "param_name" => "type",
                    "admin_label" => true,
                    "value" => array(
                        __('Featured', 'holo-shortcodes') => "featured",
                        __('Best Selling', 'holo-shortcodes') => "best-selling",
                        __('Top Rated', 'holo-shortcodes') => "top-rated",
                        __('Latest', 'holo-shortcodes') => "latest",
                        __('On Sale', 'holo-shortcodes') => "onsale"
                    ),
                    "description" => __( "Select the type of query.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Show Posts", 'holo-shortcodes' ),
                    "param_name" => "showposts",
                    "value" => '-1',
                    "description" => __( "Input the number of product that you want to display.", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Pinboard", 'holo-shortcodes'),
            "base" => "nvr_pinboard",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            "show_settings_on_create" => false,
            "js_view" => 'VcColumnView',
            'is_container' => true,
            'as_parent' => array(
                'only' => 'nvr_pin',
            ),
            'content_element' => true,
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Custom Class", 'holo-shortcodes' ),
                    "param_name" => "class",
                    "value" => "",
                    "description" => __( "Input your custom class. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Height", 'holo-shortcodes' ),
                    "admin_label" => true,
                    "param_name" => "height",
                    "value" => "",
                    "description" => __( "Input your height. (numeric only)", 'holo-shortcodes' )
                )
            )
        )
    );

    vc_map(
        array(
            "name" => __( "Pin", 'holo-shortcodes'),
            "base" => "nvr_pin",
            "class" => "",
            "category" => __( "Holo", 'holo-shortcodes'),
            'as_child' => array(
                'only' => 'nvr_pinboard',
            ),
            'content_element' => true,
            "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Custom Class", 'holo-shortcodes' ),
                    "param_name" => "class",
                    "value" => "",
                    "description" => __( "Input your custom class. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Top", 'holo-shortcodes' ),
                    "param_name" => "top",
                    "admin_label" => true,
                    "value" => "",
                    "description" => __( "Input your top position. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Left", 'holo-shortcodes' ),
                    "admin_label" => true,
                    "param_name" => "left",
                    "value" => "",
                    "description" => __( "Input your left position. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Width", 'holo-shortcodes' ),
                    "admin_label" => true,
                    "param_name" => "width",
                    "value" => "",
                    "group" => __("Outer Style", "holo-shortcodes"),
                    "description" => __( "Input your width. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Height", 'holo-shortcodes' ),
                    "admin_label" => true,
                    "param_name" => "height",
                    "value" => "",
                    "group" => __("Outer Style", "holo-shortcodes"),
                    "description" => __( "Input your height. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Background Color", 'holo-shortcodes' ),
                    "param_name" => "bgcolor",
                    "value" => "",
                    "group" => __("Outer Style", "holo-shortcodes"),
                    "description" => __( "Input your background color. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Margin", 'holo-shortcodes' ),
                    "param_name" => "margin",
                    "value" => "",
                    "group" => __("Inner Style", "holo-shortcodes"),
                    "description" => __( "Input your margin. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => __( "Padding", 'holo-shortcodes' ),
                    "param_name" => "padding",
                    "value" => "",
                    "group" => __("Inner Style", "holo-shortcodes"),
                    "description" => __( "Input your padding. (numeric only)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => __( "Border Color", 'holo-shortcodes' ),
                    "param_name" => "bordercolor",
                    "value" => "",
                    "group" => __("Inner Style", "holo-shortcodes"),
                    "description" => __( "Input your border color. (optional)", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => __( "Layer Horizontal Align", 'holo-shortcodes' ),
                    "param_name" => "alignh",
                    "value" => array(
                        __('Left', 'holo-shortcodes') => "left",
                        __('Center', 'holo-shortcodes') => "center",
                        __('Right', 'holo-shortcodes') => "right"
                    ),
                    "description" => __( "Select the horizontal align for this layer.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => __( "Layer Vertical Align", 'holo-shortcodes' ),
                    "param_name" => "alignv",
                    "value" => array(
                        __('Top', 'holo-shortcodes') => "top",
                        __('Middle', 'holo-shortcodes') => "middle",
                        __('Bottom', 'holo-shortcodes') => "bottom"
                    ),
                    "description" => __( "Select the vertical align for this layer.", 'holo-shortcodes' )
                ),
                array(
                    "type" => "textarea_html",
                    "class" => "",
                    "heading" => __( "Content", "holo-shortcodes" ),
                    "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                    "value" => __( "<p>I am test text block. Click edit button to change this text.</p>", "holo-shortcodes" ),
                    "description" => __( "Enter your content.", "holo-shortcodes" )
                )
            )
        )
    );
}

add_action('vc_before_init', 'holo_vc_map');

if(class_exists('WPBakeryShortCodesContainer')){
    class WPBakeryShortCode_nvr_pinboard extends WPBakeryShortCodesContainer {

    }
}

if( class_exists("WPBakeryShortCode") ){
    class WPBakeryShortCode_nvr_pin extends WPBakeryShortCode {

    }
}

add_action("widgets_init", "holo_shortcodes_load_widgets");
function holo_shortcodes_load_widgets() {
	register_widget("Holo_ProductCarouselWidget");
	register_widget("Holo_ProductFilterWidget");
	register_widget("Holo_BannerImageWidget");
}
?>
