<?php
	/******METER******/
if(!function_exists('nvr_meter')){
	function nvr_meter($atts, $content = null) {
		extract(shortcode_atts(array(
			'title' => 'Skills',
			'percent' => 100
		), $atts));
		
		if(!is_numeric($percent) || $percent > 100){
			$percent = 100;
		}
		
		$nvr_output = '';
		
		$nvr_output .= '<h6 class="marginsmall metertitle">'.$title.'&nbsp;<span>'.$percent.'%</span></h6>';
		$nvr_output .= '<div class="meter"><div style="width:'.esc_attr( $percent ).'%;"></div></div>';
		return $nvr_output;
	}
}
?>