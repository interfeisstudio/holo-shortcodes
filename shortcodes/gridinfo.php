<?php
	
	/* -----------------------------------------------------------------
		Grid Info
	----------------------------------------------------------------- */
	function nvr_grid_information($atts, $content = null) {
		extract(shortcode_atts(array(
			'title' 	=> '',
			'subtitle' 	=> '',
			'image'		=> '',
			'link' 		=> '',
			'linktext' 	=> '',
			'bgcolor' 	=> '#fbfbfb'
		), $atts));
		
		$nvr_style = '';
		$nvr_image = '';
		if($bgcolor!=''){
			$nvr_style .= 'background-color:'.esc_attr( $bgcolor ).';';
		}
		if($image!=''){
			$imageurl = wp_get_attachment_image_src($image, 'thumbnail');
			if($imageurl!=false){
				$nvr_image .= '<div class="gridimg" style="background-image:url('.esc_url( $imageurl[0] ).');"></div>';
			}
		}
		
		$nvr_output = '<div class="nvr_gridinfo" style="'.esc_attr( $nvr_style ).'"><div class="cellcontent"><table><tr><td class="tabletd"><div class="bannercontent">'.$nvr_image.'<h4 class="gridtitle">'.esc_html($title).'</h4><div class="gridsubtitle">'.esc_html( $subtitle ).'</div><a class="button gridbutton" href="'.esc_url( $link ).'">'.esc_html( $linktext ).'</a></div></td></tr></table></div></div>';
		return do_shortcode($nvr_output);
	}
?>