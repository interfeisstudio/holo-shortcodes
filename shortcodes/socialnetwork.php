<?php
	/* -----------------------------------------------------------------
		Social Network
	----------------------------------------------------------------- */
	function nvr_socialnetwork($atts, $content) {
		extract(shortcode_atts(array(
					"usetext" => '',
					"vertical" => '',
		), $atts));

		if(!function_exists('nvr_socialicon')){return false;}

		if($usetext=='yes'){
			$usetext = true;
		}else{
			$usetext = false;
		}

		if($vertical=='yes'){
			$vertical = true;
		}else{
			$vertical = false;
		}

		return nvr_socialicon($usetext, $vertical);
	}

	if( !function_exists('nvr_fontsocialicon')){
		function nvr_fontsocialicon(){
			$nvr_optSocialIcons = array(
				'fa-dribbble' => 'Dribbble',
				'fa-facebook' => 'Facebook',
				'fa-flickr' => 'Flickr',
				'fa-foursquare' => 'Foursquare',
				'fa-github' => 'Github',
				'fa-google-plus' => 'Google Plus',
				'fa-instagram' => 'Instagram',
				'fa-linkedin' => 'LinkedIn',
				'fa-pinterest' => 'Pinterest',
				'fa-skype' => 'Skype',
				'fa-tumblr' => 'Tumblr',
				'fa-twitter' => 'Twitter',
				'fa-vimeo-square' => 'Vimeo',
				'fa-youtube' => 'Youtube'
			);


			return $nvr_optSocialIcons;
		}
	}

	if( !function_exists('nvr_get_option')){
		 function nvr_get_option( $nvr_name, $nvr_default = false ) {
		  global $nvr_option;
		  $nvr_config = $nvr_option;
		  if ( !isset($nvr_config[$nvr_name]) ) {
		   return $nvr_default;
		  }

		  if ( isset( $nvr_config[$nvr_name] ) ) {
		   return $nvr_config[$nvr_name];
		  }

		  return $nvr_default;
		 }
	}

 if( !function_exists('nvr_socialicon')){
	 function nvr_socialicon(){

			$nvr_currenttheme = wp_get_theme();
			
			$nvr_optSocialIcons = nvr_fontsocialicon();

			$nvr_outputli = "";
			foreach($nvr_optSocialIcons as $nvr_optSocialIcon => $nvr_optSocialText){
				$nvr_sociallink = nvr_get_option( str_replace('-','_', $nvr_currenttheme->get( 'TextDomain' )).'_socialicon_'.$nvr_optSocialIcon, "" );
				if(isset($nvr_sociallink) && $nvr_sociallink!=''){
					$nvr_outputli .= '<li><a href="'.esc_url( $nvr_sociallink ).'" class="fa '.esc_attr( $nvr_optSocialIcon ).'"></a></li>'."\n";
				}
			}
			$nvr_output = "";
			if($nvr_outputli!=""){
				$nvr_output .= '<ul class="sn">';
				$nvr_output .= $nvr_outputli;
				$nvr_output .= '</ul>';
			}
			return $nvr_output;
		}
	}

?>
