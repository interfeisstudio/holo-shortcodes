<?php

	/* -----------------------------------------------------------------
		Grid Image
	----------------------------------------------------------------- */
	function nvr_grid_image($atts, $content = null) {
		extract(shortcode_atts(array(
			'title' 	=> '',
			'height'	=> '',
			'subtitle' 	=> '',
			'link' 		=> '',
			'linktext'	=> '',
			'bgcolor' 	=> '',
			'bgimage'	=> ''
		), $atts));

		$nvr_style = '';
		if($bgcolor!=''){
			$nvr_style .= 'background-color:'.esc_attr( $bgcolor ).';';
		}
		if($bgimage!=''){
			$imageurl = wp_get_attachment_image_src($bgimage, 'full');
			if($imageurl!=false){
				$nvr_style .= 'background-image:url('.esc_url( $imageurl[0] ).');';
			}
		}

		if($height){
			$nvr_style .= 'height: ' . ( preg_match( '/(px|em|\%|pt|cm)$/', $height ) ? $height : $height . 'px' ) . ';';
		}

		if($linktext!=""){
			$linktext = esc_html( $linktext) . '<i class="fa"></i>';
		}

		$nvr_output = '<div class="nvr_gridimg" style="'.esc_attr( $nvr_style ).'"><a href="'.esc_url( $link ).'" class="linkrow"><div class="cellcontent"><table><tr><td class="tabletd"><div class="bannercontent"><div class="gridsubtitle">'.$subtitle.'</div><h4 class="gridtitle">'.esc_html($title).'</h4><span class="gridlinktext">'.$linktext.'</span></div></td></tr></table></div></a></div>';
		return do_shortcode($nvr_output);
	}
?>
