<?php
	/******SLIDER******/
if(!function_exists('nvr_sliders')){
	function nvr_sliders($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' => '',
			'title' => '',
		), $atts));
		if($id!=""){
			$ids = 'id="'.esc_attr( $id ).'" ';
		}else{
			$ids = '';
		}
		$nvr_output  = '<div '.$ids.' class="minisliders flexslider">';
		
		if($title!=""){
			$nvr_output  .='<div class="titlecontainer"><h2 class="contenttitle"><span>'.$title.'</span></h2></div>';
		}
		
		$nvr_output	.= '<ul class="slides">';
		$nvr_output	.= $content;
		$nvr_output	.= '</ul>';
		$nvr_output	.= '<div class="clearfix"></div>';
		$nvr_output .= '</div>';
		return do_shortcode($nvr_output);
	}
}
if(!function_exists('nvr_slide')){
	function nvr_slide($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' 	=> '',
			'class'	=> ''
		), $atts));
		if($id!=""){
			$ids = 'id="'.esc_attr( $id ).'" ';
		}else{
			$ids = '';
		}
		$classes = 'class="slide '.esc_attr( $class ).'" ';
		
		$nvr_output  = '<li '.$ids.$classes.'>';
		$nvr_output	.= $content;
		$nvr_output	.= '</li>';
		return do_shortcode($nvr_output);
	}
}
?>