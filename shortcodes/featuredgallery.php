<?php
	if(!function_exists('nvr_featuredgallery')){
	function nvr_featuredgallery($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' => '',
			'class' => '',
			'column' => '4',
			'moreproperties' => ''
		), $atts));
		
		$nvr_initial = "nvr";
		$nvr_shortname = "novaro";
		
		global $post;
		
		if($id!=""){
			$ids = 'id="'.esc_attr( $id ).'" ';
			$theid = $id;
		}else{
			$ids = 'id="'.esc_attr( $post->ID ).'" ';
			$theid = $post->ID;
		}
		
		$nvr_custom = nvr_get_customdata($theid);
		$nvr_cf_imagegallery	= (isset($nvr_custom[$nvr_initial."_imagesgallery"][0]))? $nvr_custom[$nvr_initial."_imagesgallery"][0] : "";
		
		if($nvr_cf_imagegallery!=''){
			$nvr_attachments = $nvr_cf_imagegallery;
			$nvr_attachmentids = explode(",",$nvr_attachments);
			$nvr_qryposts = array(
				'include' => $nvr_attachmentids,
				'post_status' => 'any',
				'post_type' => 'attachment'
			);
			
			$attachments = get_posts( $nvr_qryposts );
		}else{
			$nvr_qrychildren = array(
				'post_parent' => $theid ,
				'post_status' => null,
				'post_type' => 'attachment',
				'order_by' => 'menu_order',
				'order' => 'ASC',
				'post_mime_type' => 'image'
			);
		
			$attachments = get_children( $nvr_qrychildren );
		}
		
		$qrychildren = array(
			'post_parent' => $theid,
			'post_status' => null,
			'post_type' => 'attachment',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_mime_type' => 'image'
		);
		
		$column = intval($column);
		if($column!= 1 && $column!= 2 && $column!= 3 && $column!= 4 ){
			$column = 4;
		}
		
		$typecol = "nvr-pf-col-".$column;
		$imgsize = "portfolio-image";
		
		$lipf = "";
		$idnum = 0;
		foreach ( $attachments as $att_id => $attachment ) { 
			$att_id = $attachment->ID;
			$getimage = wp_get_attachment_image_src($att_id, $imgsize, true);
			$portfolioimage = $getimage[0];
			$alttext = get_post_meta( $attachment->ID , '_wp_attachment_image_alt', true);
			$image_title = $attachment->post_title;
			$caption = $attachment->post_excerpt;
			$description = $attachment->post_content;
			$cf_thumb ='<img src="'.esc_url( $portfolioimage ).'" alt="'.esc_attr( $alttext ).'" title="'. esc_attr( $image_title ).'" class="scale-with-grid" />';
			
			$getfullimage = wp_get_attachment_image_src($att_id, 'full', true);
			$fullimage = $getfullimage[0];
			
			if($column==1){
				$classpf = 'twelve columns ';
			}elseif($column==2){
				$classpf = 'six columns ';
			}elseif($column==3){
				$classpf = 'four columns ';
			}else{
				$classpf = 'three columns ';
			}
			
			$rel = " ";

			if((($idnum+1)%$column) == 1){
				$classpf .= "alpha";
			}elseif((($idnum+1)%$column) == 0 && $idnum>0){
				$classpf .= "last";
			}else{
				$classpf .= "";
			}
			
			$lipf .='<li class="'.esc_attr( $classpf ).'">';
			$lipf .='<div class="nvr-pf-img">';
				$lipf .='<a href="'.esc_url( $fullimage ).'" data-rel="prettyPhoto['.esc_attr( $theid ).']" title="'.esc_attr( $image_title ).'">'.$cf_thumb.'</a>';
			$lipf .='</div>';
			$lipf .='</li>';
			
			$idnum++;
		}
		
		$nvr_output  = '<div '.$ids.' class="row '.esc_attr( $class ).' nvr-pf-container" '.$moreproperties.'>';
		$nvr_output	.= '<ul class="'.esc_attr( $typecol ).'">';
		$nvr_output	.= $lipf;
		$nvr_output	.= '<li class="pf-clear"></li></ul>';
		$nvr_output	.= '</div>';
		return $nvr_output;
	}
}
?>