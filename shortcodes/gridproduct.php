<?php
	
	/* -----------------------------------------------------------------
		Grid Product
	----------------------------------------------------------------- */
	function nvr_grid_product($atts, $content = null) {
		extract(shortcode_atts(array(
			'productid' => ''
		), $atts));
		
		if(!function_exists('is_woocommerce')){ return false;}
			
		$nvr_query_args = array("post_type" => "product");
		
		if($productid!='' && is_numeric($productid)){
			$nvr_query_args["p"] = $productid;
		}else{
			$nvr_query_args["name"] = sanitize_title( $productid );
		}
		$nvr_qrloop = new WP_Query($nvr_query_args);
		$nvr_licontent = "";
		if ($nvr_qrloop->have_posts()){ 
			while ($nvr_qrloop->have_posts()){ $nvr_qrloop->the_post();
				
				global $product;
				
				$nvr_licontent .= nvr_productbox($nvr_qrloop->post);

			}
			
		} /* end if($nvr_qrloop->have_posts()) */
		wp_reset_postdata();
			
		$nvr_output  = '<div class="nvr_gridproduct"><div class="cellcontent"><table><tr><td class="tabletd"><div class="bannercontent">';
			
			$nvr_output  .='<ul class="products">';
			
			$nvr_output .= $nvr_licontent;
			 
			$nvr_output .='</ul>';
		$nvr_output .= '</div></td></tr></table></div></div>';
		return do_shortcode($nvr_output);
	}
?>