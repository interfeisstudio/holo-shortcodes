<?php
if(!function_exists('nvr_login_form')){
	function nvr_login_form($attributes = null, $content = null) {
		 // get user dashboard link
		global $wpdb;
		$redirect='';
		$mess='';
	
		  $post_id=get_the_ID();
		  
		$nvr_nonce = wp_create_nonce("nvr_login_nonce");
		$return_string=' 
			<div class="login_form" id="login-div">
				<div class="loginalert" id="login_message_area" >'.$mess.'</div>
			
				<p><label for="login_user">'.__('Username', "novarostudio" ).'</label><input type="text" class="textbox" name="login_user" id="login_user" value="" size="20" /></p>
				<p><label for="login_pwd">'.__('Password', "novarostudio" ).'</label><input type="password" class="textbox" name="login_pwd" id="login_pwd" size="20" /></p>
		';
			$return_string .= '<input type="hidden" name="loginpop" id="loginpop" value="0">';
			$return_string .= '<input type="hidden" name="security-login" id="security-login" value="'.esc_attr( $nvr_nonce ).'">';
			$return_string .= '<input type="submit" value="'.esc_attr__('Login', "novarostudio" ).'" id="wp-login-but" name="submit" class="button">';
		$return_string .= '<div class="login-links">';
			
				$return_string.='<a href="'. wp_lostpassword_url() .'" id="forgot_pass">'.__('forgot password?', "novarostudio" ).'</a>
				</div>
			</div>
		';
	
			
		$nvr_nonce = wp_create_nonce("nvr_forgot_nonce");
		$return_string.='                 
			<div class="login_form" id="forgot-pass-div">
				<div class="loginalert" id="forgot_pass_area" ></div>
				<p><label for="forgot_email">'.__('Enter Your Email Address', "novarostudio" ).'</label><input type="text" class="textbox" name="forgot_email" id="forgot_email" value="" size="20" /></p>
		';
			$return_string .='<input type="hidden" id="security-forgot" name="security-forgot" value="'.esc_attr( $nvr_nonce ).'">';
			$return_string .='<input type="hidden" id="postid" value="'.esc_attr( $post_id ).'">';
			$return_string .='<input type="submit" value="'.esc_attr__('Reset Password', "novarostudio" ).'" id="wp-forgot-but" name="forgot" class="button">';   
				$return_string .='<div class="login-links"><a href="#" id="return_login">'.__('return to login', "novarostudio" ).'</a></div>
			</div>
		';
		return  $return_string;
	}
}
?>