<?php
	
	/* -----------------------------------------------------------------
		Pinboard
	----------------------------------------------------------------- */
	function nvr_pinboard($atts, $content = null, $code) {
		extract(shortcode_atts(array(
					"class" => '',
                    "height" => ''
		), $atts));
		
        if(is_numeric($height)){
            $height = 'height:'.$height.'px;';
        }

		$nvr_output = '<div class="pinboardcontainer"><div class="pinboard '.esc_attr( $class ).'" style="'. esc_attr($height) .'"><div class="pinboard_needle"><div class="pinboard_start">' . $content . '</div></div></div></div>';
		
		return do_shortcode($nvr_output);
	}

    function nvr_pinboard_pin($atts, $content = null) {
		extract(shortcode_atts(array(
            "class" => '',
            "width" => '',
            "height" => '',
            "top"   => '',
            "left"  => '',
            "alignh" => "",
            "alignv" => "",
            "bgcolor" => "",
            "margin" => "",
            "padding" => "",
            "bordercolor" => ""
		), $atts));
        
        $allowedalignh = array("left","center","right");
        $allowedalignv = array("top","middle","bottom");
        
        $style = '';
        if(in_array($alignh,$allowedalignh)){
            $class .= " pin".$alignh;
        }else{
            $class .= ' pinleft';
        }
        
        if(in_array($alignv,$allowedalignv)){
            $class .= " pin".$alignv;
        }else{
            $class .= ' pintop';
        }
        
        if(is_numeric($width)){
            $style .= 'width:'.$width.'px;';
        }
        
        if(is_numeric($height)){
            $style .= 'height:'.$height.'px;';
        }
        
        if(is_numeric($top)){
            $style .= 'top:'.$top.'px;';
        }
           
        if(is_numeric($left)){
            $style .= 'left:'.$left.'px;';
        }
        
        if($bgcolor!=""){
            $style .= 'background-color:'.$bgcolor.';';
        }
        
        $pinstyle = '';
        if($bordercolor!=""){
            $pinstyle .= 'border-color:'.$bordercolor.';';
        }
        
        if(is_numeric($margin)){
            $pinstyle .= 'margin:'.$margin.'px;';
        }
        
        if(is_numeric($padding)){
            $pinstyle .= 'padding:'.$padding.'px;';
        }
		
		$nvr_output = '<div class="thepin '.esc_attr( $class ).'" style="'. esc_attr( $style ) .'"><div class="thepincontent" style="'.esc_attr( $pinstyle ).'">' . $content . '</div></div>';
		
		return do_shortcode($nvr_output);
		
	}

?>