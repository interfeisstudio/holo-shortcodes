<?php
	/******HEADING******/
if(!function_exists('nvr_heading')){
	function nvr_heading($atts, $content = null) {
		extract(shortcode_atts(array(
			'level' => '6',
			'align' => 'center',
			'class' => ''
		), $atts));
		
		$arrH = array('1','2','3','4','5','6');
		if(!in_array($level,$arrH)){
			$level = 3;
		}
		if($align!='center' || $align!='left' || $align!='right'){
			$align = 'center';
		}
		$nvr_output = '<div class="nvr-heading '.esc_attr( $class.' '.$align ).'"><h'.$level.' class="hcont"><span>'.$content.'</span></h'.$level.'><span class="hborder"></span></div>';
		return do_shortcode($nvr_output);
	}
}
?>