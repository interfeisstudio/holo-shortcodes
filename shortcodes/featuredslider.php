<?php
	if(!function_exists('nvr_featuredslider')){
	function nvr_featuredslider($atts, $content = null) {
		extract(shortcode_atts(array(
			'id' => '',
			'class' => 'minisliders',
			'moreproperties' => ''
		), $atts));
		
		$nvr_initial = "nvr";
		$nvr_shortname = "novaro";
		
		global $post;
		
		if($id!=""){
			$ids = 'id="'.esc_attr( $id ).'" ';
			$theid = $id;
		}else{
			$ids = 'id="'.esc_attr( $post->ID ).'" ';
			$theid = $post->ID;
		}
		
		$nvr_custom = nvr_get_customdata($theid);
		$nvr_cf_imagegallery	= (isset($nvr_custom[$nvr_initial."_imagesgallery"][0]))? $nvr_custom[$nvr_initial."_imagesgallery"][0] : "";
		
		$cf_thumb2 = array(); $cf_full2 = "";
		$imgsize = "portfolio-image";
		if($nvr_cf_imagegallery!=""){
			$nvr_attachments = $nvr_cf_imagegallery;
			$nvr_attachmentids = explode(",",$nvr_attachments);
			
			foreach ( $nvr_attachmentids as $att_id ) {
				if($att_id==''){continue;}
				$getimage = wp_get_attachment_image_src($att_id, $imgsize, true);
				if($getimage){
					$portfolioimage = $getimage[0];
					$alttext = get_post_meta( $att_id , '_wp_attachment_image_alt', true);
					$cf_thumb2[] ='<img src="'.esc_url( $portfolioimage ).'" alt="'.esc_attr( $alttext ).'" title="'. esc_attr( $alttext ) .'" class="scale-with-grid" />';
					
					$getfullimage = wp_get_attachment_image_src($att_id, 'full', true);
					$fullimage = $getfullimage[0];
					
					$cf_full2 .='<li class="slide" id="'.esc_attr( $att_id ).'"><img src="'.esc_url( $fullimage ).'" alt="'.esc_attr( $alttext ).'" title="'. esc_attr( $alttext ).'" /></li>';
				}
			}
		}else{
			$qrychildren = array(
				'post_parent' => $theid,
				'post_status' => null,
				'post_type' => 'attachment',
				'orderby' => 'menu_order',
				'order' => 'ASC',
				'post_mime_type' => 'image'
			);
	
			$attachments = get_children( $qrychildren );
			
			foreach ( $attachments as $att_id => $attachment ) {
				$getimage = wp_get_attachment_image_src($att_id, $imgsize, true);
				$portfolioimage = $getimage[0];
				$alttext = get_post_meta( $attachment->ID , '_wp_attachment_image_alt', true);
				$image_title = $attachment->post_title;
				$caption = $attachment->post_excerpt;
				$description = $attachment->post_content;
				$cf_thumb2[] ='<img src="'.esc_url( $portfolioimage ).'" alt="'.esc_attr( $alttext ).'" title="'. esc_attr( $image_title ) .'" class="scale-with-grid" />';
				
				$getfullimage = wp_get_attachment_image_src($att_id, 'full', true);
				$fullimage = $getfullimage[0];
				
				$cf_full2 .='<li class="slide" id="'.esc_attr( $att_id ).'"><img src="'.esc_url( $fullimage ).'" alt="'.esc_attr( $alttext ).'" title="'. esc_attr( $image_title ).'" /></li>';
			}
		}
        
        if($cf_full2=='' && has_post_thumbnail($theid)){
            $cf_full2 .= '<li class="slide" id="'.esc_attr($theid).'">'. get_the_post_thumbnail($theid, 'full', array('class' => 'scale-with-grid')).'</li>';
        }
		
		$nvr_output  = '<div '.$ids.' class="'.esc_attr( $class ).' flexslider" '.$moreproperties.'>';
		$nvr_output	.= '<ul class="slides">';
		$nvr_output	.= $cf_full2;
		$nvr_output	.= '</ul>';
		$nvr_output	.= '</div>';
		return $nvr_output;
	}
}
?>