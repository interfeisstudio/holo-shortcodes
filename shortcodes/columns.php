<?php
	
	
	/* -----------------------------------------------------------------
		Columns shortcodes
	----------------------------------------------------------------- */
	function nvr_row($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => '',
                    "equalheight" => '',
                    "contentposition" => 'center'
		), $atts));
		
        if($equalheight=="yes"){
            $class .= ' nvrequalheight';
        }
        if($contentposition=="center"){
            $class .= ' contentcenter';
        }
		$nvr_output = '<div class="row '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	
	function nvr_one_half($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="six columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	

	function nvr_one_third($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="four columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	
	function nvr_one_fourth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="three columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_one_sixth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="two columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_two_third($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="eight columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_two_fourth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="six columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_two_sixth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="four columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_three_fourth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="nine columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_three_sixth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="six columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	
	function nvr_four_sixth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="eight columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
	
	function nvr_five_sixth($atts, $content = null) {
		extract(shortcode_atts(array(
					"class" => ''
		), $atts));
		
		$nvr_output = '<div class="ten columns '.esc_attr( $class ).'">' . $content . '</div>';
		
		return do_shortcode($nvr_output);
		
	}
?>