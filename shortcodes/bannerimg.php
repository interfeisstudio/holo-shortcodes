<?php
	/******BANNER******/
if(!function_exists('nvr_bannerimg')){
	function nvr_bannerimg($atts, $content = null) {
		extract(shortcode_atts(array(
			'src' => '',
			'link' => '',
			'margin' => '',
			'padding' => '',
			'class' => '',
			'subcontent' => ''
		), $atts));

		$nvr_style = '';
		if($margin!=''){
			$nvr_style .= 'top:'.$margin.'px;';
			$nvr_style .= 'left:'.$margin.'px;';
			$nvr_style .= 'right:'.$margin.'px;';
			$nvr_style .= 'bottom:'.$margin.'px;';
		}

		if($padding!=''){
			$nvr_style .= 'padding:'.$padding.'px;';
		}

		if($nvr_style!=''){
			$nvr_style = 'style="'.esc_attr( $nvr_style ).'"';
		}

		if($subcontent!=''){
			$nvrsubcontent = '<div class="bannersubcontent">'.$subcontent.'</div>';
		}else{
			$nvrsubcontent = '';
		}
		$nvr_output = '<div class="bannerimg '. esc_attr($class) .'"><a href="'.esc_url( $link ).'" class="linkrow"><div class="cellcontent" '.$nvr_style.'><table><tr><td>'.$nvrsubcontent.'<div class="bannercontent">'.$content.'</div></td></tr></table></div><img src="'.$src.'" alt="" /></a></div>';
		return do_shortcode($nvr_output);
	}
}
?>
