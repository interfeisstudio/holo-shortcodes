<?php
	/******HOSTING TABLE******/
if(!function_exists('nvr_hostingtable')){
	function nvr_hostingtable($atts, $content) {
		
		extract(shortcode_atts(array(
					"title" => '',
					"price" => '',
					"priceinfo" => __('Permonth', "novarostudio"),
					"buttontext" => __('Purchase Now', "novarostudio"),
					'buttonlink' => ''
		), $atts));
		
		$return_html = '';
		
		$return_html .= '<div class="hostingtable">';
			$return_html .= '<div class="hostingtitle"><h4>'.$title.'</h4></div>';
			$return_html .= '<div class="hostingprice">';
			$return_html .= $price;
			$return_html .= '<span class="priceinfo">'.$priceinfo.'</span>';
			$return_html .= '</div>';
			$return_html .= '<div class="hostingcontent">';
			$return_html .= $content;
			$return_html .= '</div>';
			if($buttonlink!=''){
				$return_html .= '<div class="hostingbutton"><a href="'.esc_url( $buttonlink ).'" class="button">'.$buttontext.'</a></div>';
			}
		$return_html .= '</div>';
		
		return $return_html;
	}
}
?>