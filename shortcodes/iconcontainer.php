<?php
	/******ICON CONTAINER******/
if(!function_exists('nvr_iconcontainer')){
	function nvr_iconcontainer($atts, $content) {
		
		extract(shortcode_atts(array(
					"iconclass" => '',
					"align" => '',
					"size" => '',
					"color" => '',
					"padding" => '',
					"radius" => '',
					'type' => '1'
		), $atts));
		
		$class = '';
		if($iconclass!=""){
			$class .=' '.$iconclass;
		}
		if($align=='right'){
			$class .=' alignright';
		}elseif($align=='center'){
			$class .=' aligncenter';
		}else{
			$class .=' alignleft';
		}
		if($type=='2'){
			$class .=' type2';
		}elseif($type=='3'){
			$class .=' type3';
		}
		
		$style = '';
		if(is_numeric($size)){
			$style .= 'font-size:'.$size.'px;';
			$style .= 'width:'.$size.'px;';
			$style .= 'height:'.$size.'px;';
			$style .= 'line-height:'.$size.'px;';
		}
		if($color!=''){
			$style .= 'border-color:'.$color.';';
			if($type=='2'){
				$style .= 'background-color:'.$color.';';
			}else{
				$style .= 'color:'.$color.';';
			}
		}
		if($padding!=''){
			$padding = preg_match('/(px|em|\%|pt|cm)$/', $padding) ? $padding : $padding.'px';
			$style .='padding:'.$padding.';';
		}
		if(is_numeric($radius)){
			$style .='border-radius:'.$radius.'px;';
			$style .='-webkit-border-radius:'.$radius.'px;';
			$style .='-moz-border-radius:'.$radius.'px;';
		}
		if($style!=''){
			$style = ' style="'.esc_attr( $style ).'"';
		}
		$return_html = '<div class="icn-container fa'.esc_attr( $class ).'"'.$style.'></div>';
		
		return $return_html;
	}
}
?>