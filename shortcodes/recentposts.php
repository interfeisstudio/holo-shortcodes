<?php
	/* Recent Posts */
if( !function_exists('nvr_recentposts') ){
	function nvr_recentposts($atts, $content = null) {
		extract(shortcode_atts(array(
					"cat" => '',
					"showposts" => '-1',
					'limitchar' => 100,
					'vertical' => 'yes'
		), $atts));

			$nvrcontclass = '';
			if($vertical=='yes'){
				$nvrcontclass .= ' verticalpost';
			}

			$nvr_output  ='<div class="nvr-recentposts '. esc_attr( $nvrcontclass ) .'">';

			$i=1;
			$nvr_argquery = array(
				'showposts' => $showposts
			);
			if($cat){
				$nvr_argquery['category_name'] = $cat;
			}

			$nvr_postqry = new WP_Query( $nvr_argquery );

			$nvr_output  .='<div class="rp-grid">';
				$nvr_output  .='<ul class="row">';

				$nvr_havepost = false;

				if( $nvr_postqry->have_posts() ){
					while ($nvr_postqry->have_posts()) : $nvr_postqry->the_post();
						$nvr_havepost = true;

						$excerpt = nvr_limit_text_excerpt( get_the_content(), 55 );

						$custom = get_post_custom( get_the_ID() );
						$cthumb = (isset($custom["carousel_thumb"][0]))? $custom["carousel_thumb"][0] : "";
						$cimgbig = (isset($custom["lightbox_img"][0]))? $custom["lightbox_img"][0] : "";

						$liclass = '';
						if($i%4==1){
							$liclass .= ' alpha';
						}elseif($i%4==0){
							$liclass .= ' last';
						}

						if($vertical=='yes'){
							$liclass .= ' twelve columns';
							$thumbnailtype = 'medium';
						}else{
							$liclass .= ' three columns';
							$thumbnailtype = 'blog-post-image';
						}

						$nvr_output  .='<li class="'. esc_attr( $liclass ).'">';
							$nvr_output .= '<div class="rp-item-container">';
								if( has_post_thumbnail( get_the_ID() ) ){
									$nvr_output  .='<div class="nvr-rp-img">';
										$nvr_output  .='<a href="'.esc_url( get_permalink() ).'" title="'.esc_attr( get_the_title() ).'">'.get_the_post_thumbnail( get_the_ID(), $thumbnailtype, array('class' => 'scale-with-grid')).'</a>';
									$nvr_output  .='</div>';
								}

								$nvr_output .= '<div class="meta-date nvrsecondfont">'. get_the_time('F d, Y').'</div>';
								$nvr_output  .='<div class="nvr-rp-text">';
									$nvr_output  .='<h4><a href="'.esc_url( get_permalink() ).'">'.get_the_title().'</a></h4>';
									$excerpt = nvr_string_limit_char( $excerpt, $limitchar );
									$nvr_output  .='<div>'.$excerpt.'</div>';
									$nvr_output  .='<div class="entry-utilities">';
										$nvr_output .= '<span class="meta-author">'.__('By', "novarostudio").'&nbsp; <a href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">'. get_the_author().'</a></span>';
										$nvr_output .= '<span class="meta-comment"><i class="fa fa-comments"></i>&nbsp; '.get_comments_number( get_the_ID() ). ' '.__('Comments', "novarostudio") .'</span>';
										$nvr_output  .='<div class="clearfix"></div>';
									$nvr_output  .='</div>';

								$nvr_output .= '</div>';

								//$nvr_output .= '<a href="'.esc_url( get_permalink() ).'" class="nvr-rp-morelink">'.__('Read More', "novarostudio").'</a>';

								$nvr_output .= '<div class="clearfix"></div>';
							$nvr_output .= '</div>';
						$nvr_output  .='</li>';

						$i++; $addclass="";
					endwhile;
				}
				wp_reset_postdata();

				 $nvr_output .='</ul>';
			 $nvr_output .='</div>';
			 $nvr_output .='</div>';
			 if($nvr_havepost){
			 	return do_shortcode($nvr_output);
			}else{
				return false;
			}
	}
}

if( !function_exists("nvr_limit_text_excerpt") ){
	function nvr_limit_text_excerpt($text, $limit) {
		$text = strip_shortcodes( strip_tags( $text ) );
	  if (str_word_count($text, 0) > $limit) {
	      $words = str_word_count($text, 2);
	      $pos = array_keys($words);
	      $text = substr($text, 0, $pos[$limit]) . '...';
	  }
	  return $text;
	}
}
?>
