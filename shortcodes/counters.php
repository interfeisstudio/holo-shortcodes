<?php

/******COUNTERS******/
if(!function_exists('nvr_counters')){
	function nvr_counters($atts, $content = null) {
		$nvr_initial = "nvr";
		$nvr_shortname = "novaro";
		
		extract(shortcode_atts(array(
			'color' => '',
			'align' => '',
			'size' => '',
			'class' => ''
		), $atts));
		
		$style = '';
		if($color!=''){
			$style .= 'color:'.$color.';';
		}
		if($align!=''){
			$style .= 'text-align:'.$align.';';
		}
		if(is_numeric($size)){
			$style .= 'font-size:'.$size.'px;';
			$style .= 'height:'.$size.'px;';
			$style .= 'line-height:'.$size.'px;';
		}
		$style = 'style="'.esc_attr( $style ).'"';
		$nvr_output = '<div class="counters '.esc_attr( $class ).'" '.$style.'>'.$content.'</div>';
		return do_shortcode($nvr_output);
	}
	
}
?>