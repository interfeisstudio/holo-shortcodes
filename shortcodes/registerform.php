<?php
if(!function_exists('nvr_register_form')){
	function nvr_register_form($attributes = null, $content = null) {
	 
		 $nvr_nonce = wp_create_nonce("nvr_register_nonce");
		 $return_string='
			  <div class="login_form">
				   <div class="loginalert" id="register_message_area" ></div>
				   
					<p><label for="user_login_register">'.esc_html__('Username', "novarostudio" ).'</label><input type="text" name="user_login_register" id="user_login_register" class="textbox" value="" size="20" /></p>
					<p><label for="user_email_register">'.esc_html__('Email', "novarostudio" ).'</label><input type="text" name="user_email_register" id="user_email_register" class="textbox" value="" size="20" /></p>
					 
					<p id="reg_passmail">'.__('A password will be e-mailed to you', "novarostudio" ).'</p>
				 
					<input type="hidden" id="security-register" name="security-register" value="'.esc_attr( $nvr_nonce ).'">
					<p class="submit"><input type="submit" name="wp-submit" id="wp-submit-register" class="button" value="'.__('Register', "novarostudio" ).'" /></p>
					
			</div>
						 
		';
		 return  $return_string;
	}
}
?>