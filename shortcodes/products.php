<?php
	/******WOOCOMMERCE PRODUCT CAROUSEL******/
if(!function_exists('nvr_productcarousel')){
	function nvr_productcarousel($atts, $content = null) {
		extract(shortcode_atts(array(
			"title" => '',
			"cat" => '',
			"class" => '',
			"type" => 'featured',
			"showposts" => '-1'
		), $atts));
		
			if($type!='featured' && $type!='top-rated' && $type!='best-selling' && $type!='latest' && $type!='onsale'){
				$type = 'featured';
			}
			
			$nvr_outputtitle = '';
			if($title!=''){
				$nvr_outputtitle = '<div class="titlecontainer"><h3><span>'. $title .'</span></h3></div>';
				$class .= ' hastitle';
			}
			
			$nvr_output  ='<div class="prodcarousel '. esc_attr($class) .' woocommerce">';
			$nvr_output .= $nvr_outputtitle;
			
			global $woocommerce, $woocommerce_loop;
	
			$nvr_number = (int) $showposts;
			if ( !is_numeric($nvr_number) || $nvr_number < 1){
				$nvr_number = -1;
			}
			
			$nvr_query_args = nvr_productquery($nvr_number, $type, $cat);
	
			$nvr_qrloop = new WP_Query($nvr_query_args);
			$nvr_licontent = "";
			$nvr_allterms = array();
			$nvr_havepost = false;
			if ($nvr_qrloop->have_posts()){ 
				while ($nvr_qrloop->have_posts()){ $nvr_qrloop->the_post();
					$nvr_havepost = true;
					global $product;
					
					$nvr_licontent .= nvr_productbox($nvr_qrloop->post);
	
				}
				
			} /* end if($nvr_qrloop->have_posts()) */
			
			wp_reset_postdata();
			
			$nvr_output  .='<div class="flexslider-carousel row">';
				$nvr_output  .='<ul class="slides products">';
				
				$nvr_output .= $nvr_licontent;
				 
				$nvr_output .='</ul>';
			$nvr_output .='</div>';
			$nvr_output .='</div>';
			
			if($nvr_havepost){
				return $nvr_output;
			}else{
				return false;
			}
	}
}

if(!function_exists('nvr_productfilter')){
	function nvr_productfilter($atts, $content = null) {
		extract(shortcode_atts(array(
					"id" => '',
					"class" => '',
					"title" => '',
					"col" => '',
					"cat" => '',
					"type" => 'featured',
					"nofilter" => 'no',
					"showposts" => '12'
		), $atts));
		
		if($type!='featured' && $type!='top-rated' && $type!='best-selling' && $type!='latest' && $type!='onsale'){
			$type = 'featured';
		}
		
		if($col!=2 && $col!=3 && $col!=4){
			$col = '';
		}
		
		
		
		global $woocommerce, $woocommerce_loop;
		
		// Store column count for displaying the grid
		if ( empty( $woocommerce_loop['columns'] ) )
			$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );

		$nvr_number = (int) $showposts;
		if ( !is_numeric($nvr_number)){
			$nvr_number = 12;
		}
		
		$nvr_query_args = nvr_productquery($nvr_number, $type, $cat);
		
		$nvr_qrloop = new WP_Query($nvr_query_args);
		$nvr_output = $content = "";
		$nvr_allterms = array();
		if ($nvr_qrloop->have_posts()){ 

			while ($nvr_qrloop->have_posts()){ $nvr_qrloop->the_post(); setup_postdata($nvr_qrloop->post);
				global $product;
				
				// Store loop count we're currently on
				if ( empty( $woocommerce_loop['loop'] ) )
					$woocommerce_loop['loop'] = 0;
				
				// Ensure visibilty
				if ( ! $product->is_visible() )
					continue;
				
				// Increase loop count
				$woocommerce_loop['loop']++;
				
				$ptermsobj = get_the_terms($product->id,'product_cat');
				$pterms = "";
				if(!is_wp_error($ptermsobj) && $ptermsobj!=false){
					$ptermsarr = array();
					foreach($ptermsobj as $ptermobj){
						$ptermsarr[] = $ptermobj->slug;
						if(!in_array($ptermobj->slug, $nvr_allterms)){
							$nvr_allterms[] = $ptermobj->slug;
						}
					}
					if(count($ptermsarr)) $pterms = implode(" ",$ptermsarr);
				}
				
				$content .= nvr_productbox($nvr_qrloop->post);

            }
			
		} /* end if($nvr_qrloop->have_posts()) */
		
		if($id!=''){
			$id = 'id="'.esc_attr( $id ).'"';
		}
		
		$nvr_outputtitle = '';
		if($title!=''){
			$nvr_outputtitle = '<div class="titlecontainer"><h3><span>'. $title .'</span></h3></div>';
			$class .= ' hastitle';
		}
		
		if($col!=''){
			$class .= ' pcol'.$col;
		}
		
		$nvr_output .= '<div '.$id.' class="pfilter_container '. esc_attr( $class ).' woocommerce">';
			$nvr_output .= $nvr_outputtitle;
			$titleoutput = "";
			
			if($cat){
				$nvr_allterms = explode(",",$cat);
			}
			
			if(count($nvr_allterms)>0 && $nofilter!='yes'){
				$titleoutput .= '<div class="filterlist">';
				$titleoutput .= '<ul class="isotope-filter option-set clearfix " data-option-key="filter">';
					$filtersli = '';
					$numli = 1;
					foreach($nvr_allterms as $theterm){
						$termobj = get_term_by("slug", $theterm, 'product_cat');
						if($termobj==false){continue;}
						
						if($numli==1){
							$liclass = 'omega';
						}else{
							$liclass = '';
						}
						$filtersli .= '<li class="'. esc_attr( $liclass ).'"><a href="#filter" data-option-value=".product-cat-'.esc_attr( $termobj->slug ).'">'.$termobj->name.'</a></li>';
						$numli++;
					}
					$filtersli = '<li class="selected allcat"><a href="#filter" data-option-value="*">'. __('All Categories', "novarostudio" ).'</a></li>'.$filtersli;
					$titleoutput .= $filtersli;
				$titleoutput .= '</ul>';
				$titleoutput .= '</div>';
			}
			
			if($titleoutput!=""){
				$nvr_output .= $titleoutput;
			}
			$nvr_output .= '<div class="clearfix"></div>';
			$nvr_output .= '<div class="product_filter">';
				$nvr_output .= '<ul class="products">';
					$nvr_output .= $content;
				$nvr_output .= '</ul>';
			$nvr_output .= '</div>';
			
		$nvr_output .= '</div>';
		
        wp_reset_postdata();
		
		return trim( do_shortcode( shortcode_unautop( $nvr_output ) ) );
	}
}
?>