<?php
	/******BORDER SEPARATOR******/
if(!function_exists('nvr_borderseparator')){
	function nvr_borderseparator($atts, $content = null) {
		extract(shortcode_atts(array(
			'width' => 100,
			'height' => 2,
			'color' => '',
			'align' => 'left',
			'class' => ''
		), $atts));
		
		if(!is_numeric($width)){
			$width = 100;
		}
		
		if(!is_numeric($height)){
			$height = 2;
		}
		
		if($align!='left' && $align!='right' && $align!='center'){
			$align = 'left';
		}
		
		$colortext = '';
		if(!empty($color)){
			$colortext = 'background-color:'.$color.';';
		}
		$nvr_style = 'width:'.$width.'px; height:'.$height.'px; '.$colortext;
		$nvr_output = '';
		$nvr_output .= '<div class="bordersep '.esc_attr( $class.' '.$align ).'"><div class="bordershow" style="'.esc_attr( $nvr_style ).'"></div><div class="clearfix"></div></div>';
		return $nvr_output;
	}
}
?>